//
//  BaseTabBarController.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 21/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {

    @IBInspectable var defaultIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = defaultIndex
    }

}
