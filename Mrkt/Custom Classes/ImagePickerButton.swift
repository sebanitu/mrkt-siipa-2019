//
//  ImagePickerButton.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 16/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import Foundation
import UIKit

class ImagePickerButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 10
          self.imageView?.layer.cornerRadius = 10
          self.layer.masksToBounds = true
          self.layer.borderWidth = 1
          self.layer.borderColor = UIColor(named: "Shamrock")?.cgColor
    }
}

