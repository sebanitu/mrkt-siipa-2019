//
//  RoundButton.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 10/11/2019.
//  Copyright © 2019 Sebastian Nitu. All rights reserved.
//

import UIKit

class RoundButton: UIButton {
   
   override init(frame: CGRect){
       super.init(frame: frame)
    layer.masksToBounds = true
   }
   
   required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    layer.masksToBounds = true

   }
   
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
           return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
