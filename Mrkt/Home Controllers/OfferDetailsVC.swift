//
//  OfferDetailsVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 21/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class OfferDetailsVC: UIViewController {

    var offers = [String]()
    var userInfo = [String:Any]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        //retrieveUserInfo()
    }
    
//    func retrieveUserInfo(){
//        var data = [String:Any]()
//        for user in offers {
//                Firestore.firestore().collection(USERS_REF).document(user).getDocument { (snapshot, error) in
//                    if let error = error {
//                        debugPrint("error getting user data: \(error)")
//                    } else {
//                        guard let newData = snapshot?.data() else {return}
//                        for (key, value) in newData {
//                            data.updateValue(value, forKey: key)
//                        }
//                    }
//                }
//
//            }
//        userInfo = data
//        print(userInfo["username"])
//        }

}

extension OfferDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        offers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "offersDetailsCell") as? OfferDetailsCell {
         return cell
        } else {
            return UITableViewCell()
        }
    }
    
    
}
