//
//  AddItemVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 12/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit

class AddItemVC: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet weak var listingNameText: UITextField!
    @IBOutlet weak var listingBrandText: UITextField!
    @IBOutlet weak var listingPrice: UITextField!
    
    //MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoAddListingDetails" {
            if let destinationVC = segue.destination as? AddItemDetailsVC {
                guard let name = listingNameText.text, let brand = listingBrandText.text, let price = listingPrice.text else { return }
                
                destinationVC.listingPrice = Int(price) ?? 0
                destinationVC.listingName = name
                destinationVC.listingBrand = brand
            }
        }
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if listingNameText.text?.isEmpty ?? true || listingBrandText.text?.isEmpty ?? true || listingPrice.text?.isEmpty ?? true {
            let alert = UIAlertController(title: "Câmpuri obligatorii", message: "Toate câmpurile sunt obligatorii!", preferredStyle: .alert)
            let action = UIAlertAction(title: "Am înțeles!", style: .cancel)
            alert.addAction(action)
            self.present(alert, animated: true)
        } else {
            performSegue(withIdentifier: "gotoAddListingDetails", sender: self)
        }
    }
}
