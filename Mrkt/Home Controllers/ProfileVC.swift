//
//  ProfileVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 21/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import Kingfisher

class ProfileVC: UIViewController {
    @IBOutlet weak var profileImage: UIButton!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    
    private var usersListener: ListenerRegistration!
    private var usersCollectionRef: CollectionReference!
    private var handle: AuthStateDidChangeListenerHandle?
    private let profileImagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.layer.cornerRadius = profileImage.frame.width / 2
        profileImage.clipsToBounds = true
        profileImage.layer.borderWidth = 4
        profileImage.layer.borderColor = UIColor(named: "Brick Red")?.cgColor
        profileImage.contentMode = .scaleAspectFit
        usersCollectionRef = Firestore.firestore().collection(USERS_REF)
        setListener()
        profileImagePicker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if phoneNumber.text == "" {
            handleAlert()
        }
    }
    
    func setListener() {
        let firebaseAuth = Auth.auth()
        guard let user = firebaseAuth.currentUser else {return}
        usersListener = usersCollectionRef.document(user.uid).addSnapshotListener({ (snapshot, error) in
            if let error = error {
                debugPrint("Error retrieving user: \(error)")
            } else {
                guard let snap = snapshot, let data = snap.data() else { return }
                self.username.text = data["username"] as? String ?? "unknown"
                self.phoneNumber.text = data["phoneNumber"] as? String ?? ""
                if self.phoneNumber.text == "" {
                    self.handleAlert()
                }
                if let stringURL = data[PROFILE_PIC_URL] as? String {
                    if let url = URL(string: stringURL) {
                        let resource  = ImageResource(downloadURL: url)
                        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
                                switch result {
                                case .success(let value):
                                    self.profileImage.setBackgroundImage(value.image, for: .normal)
                                    self.profileImage.setTitle("", for: .normal)
                                case .failure(let error):
                                    print("Error setting profile image: \(error)")
                                }
                            }
                    }
                    
                } else {
                    return
                }
            }
        })
    }
    @IBAction func signOutButtonTapped(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            debugPrint("Error sigining out: \(signOutError)")
        }
    }
    
    @IBAction func addPorfileImageTapped(_ sender: Any) {
        present(profileImagePicker, animated: true)
    }
    
    @IBAction func deleteAccountButtonTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Ștergere cont!", message: "Atenție! Ștergerea contului este un proces ireversibil, toate informațiile asociate vor fi șterse definitiv!", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Anuleză", style: .cancel, handler: nil))
        
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "Introdu parola contului tău"
            textField.isSecureTextEntry = true
        })
        
        let firebaseAuth = Auth.auth()
        let currentUser = firebaseAuth.currentUser
        
        let deleteAction = UIAlertAction(title: "Sterge Cont", style: .destructive) { (action) in
            if let text = alert.textFields?.first?.text {
                firebaseAuth.signIn(withEmail: (currentUser?.email)!, password: text) { (authData, error) in
                    if let error = error {
                        debugPrint("Error re-signing in user: \(error)")
                    } else {
                        authData?.user.delete(completion: { (error) in
                            if let error = error {
                                debugPrint("Error deleting account \(error)")
                            } else {
                                
                            }
                        })
                    }
                }
            }
        }
        alert.addAction(deleteAction)
        self.present(alert, animated: true)
    }
    
    func handleAlert() {
        let alert = UIAlertController(title: "Lipsă număr de telefon!", message: "Un număr de telefon este necesar pentru a putea posta anunțuri!", preferredStyle: .alert)
        
        let dismissAction = UIAlertAction(title: "Mai târziu", style: .cancel) { (action) in
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeTabBar") as! UITabBarController
            appDelegate.window?.rootViewController = initialViewController
            appDelegate.window?.makeKeyAndVisible()
            initialViewController.selectedIndex = 0
        }
        
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "0740834567"
        })
        
        alert.addAction(dismissAction)

        alert.addAction(UIAlertAction(title: "Salvează", style: .default, handler: { action in
            let firebaseAuth = Auth.auth()
            guard let user = firebaseAuth.currentUser else {return}
            
            if let text = alert.textFields?.first?.text{
                Firestore.firestore().collection(USERS_REF).document(user.uid).setData([
                    PHONE_NUMBER : text,
                ], merge: true) { (error) in
                    if let error = error {
                        debugPrint("Error setting data for user: \(error.localizedDescription)")
                    } else {
                        return
                    }
                }
            }
        }))

        self.present(alert, animated: true)
    }
    
    func uploadProfileImage(image: UIImage, name: String, filePath: String) {
        guard let imageData: Data = image.jpegData(compressionQuality: 0.1) else {
            return
        }

        let storageRef = Storage.storage().reference().child(filePath).child(name)
        
        print("Reference path is: \(filePath)")

        storageRef.putData(imageData, metadata: nil){ (metaData, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            let user = Auth.auth().currentUser
            storageRef.downloadURL(completion: { (url: URL?, error: Error?) in
                Firestore.firestore().collection(USERS_REF).document(user!.uid).setData([
                    PROFILE_PIC_URL : url!.absoluteString,
                ], merge: true) { (error) in
                    if let error = error {
                        debugPrint("Error setting data for user: \(error.localizedDescription)")
                    } else {
                        return
                    }
                }
            })
        }
    }
}

extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    guard let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
    self.profileImage.setBackgroundImage(pickedImage, for: .normal)
   
    if let profileImage = profileImage.backgroundImage(for: .normal) {
        let newUID = UUID().uuidString
        uploadProfileImage(image: profileImage, name: newUID, filePath: IMAGES)
        self.profileImage.setTitle("", for: .normal)
    } else {
        return
    }
    dismiss(animated: true, completion: nil)
    }
}

