//
//  SellingVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 21/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class SellingVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var listingsCollectionRef: CollectionReference!
    private var listingsListener: ListenerRegistration!
    private var listings = [Listing]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        listingsCollectionRef = Firestore.firestore().collection(LISTINGS_REF)
        tableView.backgroundColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if listingsListener != nil {
            listingsListener.remove()
        }
        self.setListener()
    }
    
    func setListener() {
        let auth = Auth.auth()
        let user = auth.currentUser
        listingsListener = listingsCollectionRef
            .whereField("ownerId", isEqualTo: user?.uid)
            .order(by: TIMESTAMP, descending: true).addSnapshotListener({ (snapshot, error) in
                if let error = error {
                debugPrint("Error fetching documents: \(error)")
            } else {
                self.listings = Listing.parseData(snapshot: snapshot)
                self.tableView.reloadData()
            }
        })
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOfferDetails" {
            guard let indexPath = sender as? IndexPath else { return }
            if let destinationVC = segue.destination as? OfferDetailsVC {
                guard let offers = listings[indexPath.row].offers else { return  }
                let filteredOffers = offers.filter({$0 != ""}).map({$0})
                destinationVC.offers = filteredOffers
            }
        }
    }
}

extension SellingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "sellingSell") as? SellingListingCell {
            cell.configureCell(listing: listings[indexPath.row])

            // TODO: Extract to cell
            guard let offers = listings[indexPath.row].offers else { return cell }
            let filteredOffers = offers.filter({$0 != ""}).map({$0})
            let numOffers = filteredOffers.count
            let numOffersStr = String(filteredOffers.count)
            cell.numberOfOffers.setTitle("Oferte: \(numOffersStr)", for: .normal)
            if numOffers > 0 {
                cell.numberOfOffers.backgroundColor = UIColor(named: "Shamrock")
            }
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       guard let offers = listings[indexPath.row].offers else { return}
        let filteredOffers = offers.filter({$0 != ""}).map({$0})
        
//        if filteredOffers.count > 0 {
//            self.performSegue(withIdentifier: "toOfferDetails", sender: indexPath)
//        }
    }
}
