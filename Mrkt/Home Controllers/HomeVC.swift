//
//  HomeVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 12/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import Kingfisher

class HomeVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: Variables
    
    private var listings = [Listing]()
    private var listingsCollectionRef: CollectionReference!
    private var listingsListener: ListenerRegistration!
    private let spacing:CGFloat = 16.0
    private var handle: AuthStateDidChangeListenerHandle?
    private var listingImages = [String]()
    
    
    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        listingsCollectionRef = Firestore.firestore().collection(LISTINGS_REF)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if listingsListener != nil {
            listingsListener.remove()
        }
        self.setListener()
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user == nil {
                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                let loginVC = storyboard.instantiateViewController(identifier: "loginVC")
                loginVC.modalPresentationStyle = .fullScreen
                self.present(loginVC, animated: true, completion: nil)
            } else {
                self.setListener()
            }
        })
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        
    }
    
    @IBAction func signOutButtonTapped(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            debugPrint("Error sigining out: \(signOutError)")
        }
    }
    
    func setListener() {
        listingsListener = listingsCollectionRef
            .whereField("status", isEqualTo: 1)
            .order(by: TIMESTAMP, descending: true).addSnapshotListener({ (snapshot, error) in
            if let error = error {
                debugPrint("Error fetching documents: \(error)")
            } else {
                self.listings = Listing.parseData(snapshot: snapshot)
                self.collectionView.reloadData()
            }
        })
           
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToItemDetails" {
            guard let indexPath = sender as? IndexPath else { return }
            if let destinationVC = segue.destination as? ItemDetailsVC {
                destinationVC.listingItem = listings[indexPath.row]
            }
        }
    }

}


    // MARK: Extensions

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Collection listings count: \(listings.count)")
        return listings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListingCell", for: indexPath) as? ListingCell {
            cell.configureCell(listing: listings[indexPath.row])
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerView", for: indexPath)
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToItemDetails", sender: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/2) - 28, height: 300)
    }

}
