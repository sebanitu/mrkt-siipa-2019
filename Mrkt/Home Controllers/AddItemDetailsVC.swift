//
//  AddItemDetailsVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 12/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth

class AddItemDetailsVC: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet weak var listingDescription: UITextView!
    @IBOutlet weak var firstImageButton: UIButton!
    @IBOutlet weak var secondImageButton: UIButton!
    @IBOutlet weak var thirdImageButton: UIButton!
    @IBOutlet weak var fourthImageButton: UIButton!
    @IBOutlet weak var addButton: RoundShadowButton!
    
    
    
    // MARK: Variables
    var listingName: String = ""
    var listingBrand: String = ""
    var listingPrice: Int = 0
    private let cornerRadius: CGFloat = 10
    private let borderThickness:CGFloat = 1
    private let firstImagePicker = UIImagePickerController()
    private let secondImagePicker = UIImagePickerController()
    private let thirdImagePicker = UIImagePickerController()
    private let fourthImagePicker = UIImagePickerController()
    private let userDefaults = UserDefaults.standard


    private var listingImages = [UIImage]()
    private var imageURLs = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        firstImagePicker.delegate = self
        secondImagePicker.delegate = self
        thirdImagePicker.delegate = self
        fourthImagePicker.delegate = self
        listingDescription.delegate = self
        listingDescription.layer.cornerRadius = 10
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if userDefaults.value(forKey: "newListingDescription") == nil {
            listingDescription.text = "Adaugă o descriere..."
        } else {
            listingDescription.text = userDefaults.value(forKey: "newListingDescription") as! String
        }
    }
    
    
    @IBAction func addButtonTapped(_ sender: Any) {
        if listingDescription.text != "" && listingDescription.text != "Adaugă o descriere..." && listingDescription.text.count >= 50 {
            guard let listingDescription = listingDescription.text, let currentUser = Auth.auth().currentUser else { return }
            let firestoreRef = Firestore.firestore().collection(LISTINGS_REF).addDocument(data: [
                BRAND : listingBrand,
                DESCRIPTION : listingDescription,
                NAME : listingName,
                PRICE : listingPrice,
                STATUS : 1,
                TIMESTAMP : FieldValue.serverTimestamp(),
                OWNER_ID: currentUser.uid
                ]
            ) { (error) in
                if let error = error {
                    debugPrint("Error adding new listing: \(error.localizedDescription)")
                }
            }
            if self.listingImages.count > 0 {
                let activityIndicator = UIActivityIndicatorView()
                activityIndicator.style = UIActivityIndicatorView.Style.large
                activityIndicator.color = UIColor.white
                activityIndicator.backgroundColor = UIColor.black.withAlphaComponent(0.8)
                activityIndicator.layer.cornerRadius = 10
                
                
                self.view.addSubview(activityIndicator)
                
                activityIndicator.frame = CGRect(x: self.view.center.x - 60.0, y: self.view.center.y - 60.0, width: 120.0, height: 120.0);
                activityIndicator.startAnimating()
                
                self.addButton.isEnabled = false
                
                var imageURLs = [String]()
                for image in self.listingImages {
                    let data = image.jpegData(compressionQuality: 1)
                    let imageName = UUID().uuidString
                    let imageRef = Storage.storage().reference().child(IMAGES).child(imageName)
                    imageRef.putData(data!, metadata: nil, completion: { (metadata, error) in
                        if let error = error {
                            print("Something went wrong: \(error.localizedDescription)")
                            return
                        } else {
                            imageRef.downloadURL { (url, error) in
                                guard let downloadURL = url else {
                                    return
                                }
                                imageURLs.append(downloadURL.absoluteString)
                                print("Appending URL: \(downloadURL.absoluteString)")
                                print("Image urls count: \(imageURLs.count)")
                                if self.listingImages.count == imageURLs.count {
                                    Firestore.firestore().collection(LISTINGS_REF).document(firestoreRef.documentID).setData(["images" : imageURLs], merge: true) { (error) in
                                        if error != nil {
                                            print(error!.localizedDescription)
                                        } else {
                                            activityIndicator.removeFromSuperview()
                                            self.userDefaults.removeObject(forKey: "newListingDescription")
                                            self.dismiss(animated: true, completion: nil)
                                        }
                                    }
                                }
                            }
                        }
                    })
                }
            }
        } else {
            let alertController = UIAlertController(title: "Descriere incorectă", message: "O descriere de cel puțin 50 de caractere este necesară pentru a publica anunțul!", preferredStyle: .alert)
            let action = UIAlertAction(title: "Am înțeles", style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func imagebuttonTapped(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            present(firstImagePicker, animated: true)
        case 2:
            present(secondImagePicker, animated: true)
        case 3:
            present(thirdImagePicker, animated: true)
        default:
            present(fourthImagePicker, animated: true)
        }
    }
}

//MARK: Extensions
extension AddItemDetailsVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        listingDescription.text = ""
        listingDescription.textColor = UIColor.darkGray
    }
    
    func textViewDidChange(_ textView: UITextView) {
        userDefaults.set(textView.text, forKey: "newListingDescription")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
         userDefaults.set(textView.text, forKey: "newListingDescription")
     }
}

extension AddItemDetailsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        
        if picker == firstImagePicker {
            firstImageButton.setBackgroundImage(pickedImage, for: .normal)
            firstImageButton.setTitle("", for: .normal)
            listingImages.append(pickedImage)
        } else if picker == secondImagePicker {
            secondImageButton.setBackgroundImage(pickedImage, for: .normal)
            secondImageButton.setTitle("", for: .normal)
            listingImages.append(pickedImage)
        } else if picker == thirdImagePicker {
            thirdImageButton.setBackgroundImage(pickedImage, for: .normal)
            thirdImageButton.setTitle("", for: .normal)
            listingImages.append(pickedImage)
        } else if picker == fourthImagePicker {
            fourthImageButton.setBackgroundImage(pickedImage, for: .normal)
            fourthImageButton.setTitle("", for: .normal)
            listingImages.append(pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
}
