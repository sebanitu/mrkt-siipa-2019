//
//  ItemDetailsVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 18/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import Kingfisher

class ItemDetailsVC: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var listingName: UILabel!
    @IBOutlet weak var listingBrand: UILabel!
    @IBOutlet weak var listingDescription: UITextView!
    @IBOutlet weak var sellerName: UILabel!

    
    //MARK: Variables
    
    var listingItem: Listing?
    var usersCollectionRef: CollectionReference!
    var storage: Storage!
    
    //MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayItem()
        usersCollectionRef = Firestore.firestore().collection(USERS_REF)
        galleryCollectionView.delegate = self
        galleryCollectionView.dataSource = self
        
        // TODO: Extract to method:
        guard let listingItem = listingItem else { return }
           
           let userRef = usersCollectionRef.document(listingItem.ownerId)
           userRef.getDocument { (documentSnapshot, error) in
               if let error = error {
                   debugPrint("Error accoured retrieving document: \(error)")
               } else {
                guard let snapshot = documentSnapshot else { return }
                self.sellerName.text = "Vandut de: \(snapshot.get("username") as? String ?? "")"
               }
           }

    }
    
    @IBAction func buyButtonTapped(_ sender: Any) {
    }
    
    @IBAction func dismissButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func displayItem(){
        guard let item = listingItem else { return }
        listingName.text = item.name
        listingBrand.text = item.brand
        listingDescription.text = item.description
        
        // Text View Layout
        listingDescription.translatesAutoresizingMaskIntoConstraints = true
        listingDescription.sizeToFit()
        listingDescription.isScrollEnabled = false
    }
}

extension ItemDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        (listingItem?.imageURLs?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath) as? GalleryCell,
            // TODO: Handle optionals!!!
            let url = URL(string: (listingItem?.imageURLs![indexPath.row])!) {
                let resource = ImageResource(downloadURL: url)
                KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
                    switch result {
                    case .success(let value):
                        cell.galleryImage.image = value.image
                    case .failure(let error):
                        print("Error downloading image: \(error)")
                    }
                }
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
