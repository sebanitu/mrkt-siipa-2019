//
//  UIStoryboard+Extensions.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 09/11/2019.
//  Copyright © 2019 Sebastian Nitu. All rights reserved.
//

import UIKit

extension UIStoryboard {

    func instantiateViewController<T>(ofType type: T.Type) -> T {
        let identifier = String(describing: type)
        guard let viewController = instantiateViewController(withIdentifier: identifier) as? T else {
            fatalError("Cannot instantiate view controller")
        }
        return viewController
    }

}

extension UIStoryboard {

    static let login = UIStoryboard(name: "Login", bundle: nil)

}
