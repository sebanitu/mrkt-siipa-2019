//
//  RegisterVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 17/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit
import Firebase

class RegisterVC: UIViewController {
    @IBOutlet var tapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet weak var emailTextField: UnderlineTextField!
    @IBOutlet weak var usernameTextField: UnderlineTextField!
    @IBOutlet weak var passwordTextField: UnderlineTextField!
    @IBOutlet weak var confirmPasswordTextField: UnderlineTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tapGestureRecognizer.addTarget(self, action: #selector(dismissKeyboard))
    }
        
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    @IBAction func createButtonTapped(_ sender: Any) {
        guard let email = emailTextField.text, let username = usernameTextField.text, let password = passwordTextField.text, let confirmedPassword = confirmPasswordTextField.text else { return }
        
        if password == confirmedPassword {
            Auth.auth().createUser(withEmail: email, password: password) { (authData, error) in
                if let error = error {
                    self.present(self.createAlert(withTite: "Eroare creare cont!", andMessage: error.localizedDescription), animated: true) {
                        self.usernameTextField.text = ""
                    }
                    debugPrint("Error creating user: \(error.localizedDescription)")
                } else {
                    let changeRequest = authData?.user.createProfileChangeRequest()
                    changeRequest?.displayName = username
                    changeRequest?.commitChanges(completion: { (error) in
                        if let error = error {
                            debugPrint("Error comitting chnages for displayName: \(error.localizedDescription)")
                        }
                    })
                }
                
                guard let userId = authData?.user.uid else { return }
                Firestore.firestore().collection(USERS_REF).document(userId).setData([
                    USERNAME : username,
                    DATE_CREATED : FieldValue.serverTimestamp(),
                ]) { (error) in
                    if let error = error {
                        debugPrint("Error setting data for user: \(error.localizedDescription)")
                    } else {
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let tabBarVC = storyboard.instantiateViewController(identifier: "homeTabBar")
                        tabBarVC.modalPresentationStyle = .fullScreen
                        self.present(tabBarVC, animated: true, completion: nil)
                    }
                }
            }
        } else {
            self.present(createAlert(withTite: "Veridică parola!", andMessage: "Parolele introdue nu se potrivesc!"), animated: true) {
                self.passwordTextField.text = ""
                self.confirmPasswordTextField.text = ""
            }
            return
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createAlert(withTite title: String, andMessage message:String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Am înțeles", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }
}
