//
//  LoginVC.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 30/10/2019.
//  Copyright © 2019 Sebastian Nitu. All rights reserved.
//

import UIKit
import Firebase

class LoginVC: UIViewController {
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var emailTextField: UnderlineTextField!
    @IBOutlet weak var passwordTextField: UnderlineTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tapGesture.addTarget(self, action: #selector(dismissKeyboard))
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }

    @IBAction func loginButtonTapped(_ sender: Any) {
        guard let email = emailTextField.text, let password = passwordTextField.text else { return }
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                debugPrint("Error signing in: \(error)")
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func registerButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "goToSimpleRegister", sender: self)
    }
}

