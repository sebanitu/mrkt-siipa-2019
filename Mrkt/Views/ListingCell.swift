//
//  ListingCell.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 12/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit
import Kingfisher

class ListingCell: UICollectionViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var listingTitle: UILabel!
    @IBOutlet weak var listingBrand: UILabel!
    @IBOutlet weak var listingPrice: UILabel!
    @IBOutlet weak var listingHeroImage: UIImageView!
    
    //MARK: Variables
    
    private var listing: Listing!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        listingHeroImage.layer.cornerRadius = 10
    }
    
    func configureCell(listing: Listing){
        self.listing = listing
        listingTitle.text = listing.name
        listingBrand.text = listing.brand
        listingPrice.text = String(listing.price) + " RON"
        
        guard let urls = listing.imageURLs, let url = URL(string: listing.imageURLs?[0] ?? "Text") else { return }
        
        let resource = ImageResource(downloadURL: url)
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                self.listingHeroImage.image = value.image
            case .failure(let error):
                print("Error downloading image: \(error)")
            }
        }
    }
}
