//
//  SellingListingCell.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 21/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit

class SellingListingCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var publishDate: UILabel!
    @IBOutlet weak var numberOfOffers: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        numberOfOffers.layer.cornerRadius = 10
    }
    
    func configureCell(listing: Listing) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd, MMM"
        let postingDate = dateFormatter.string(from: listing.timestamp)
       
        self.name.text = listing.name
        self.publishDate.text = "Publicat pe: \(postingDate)"
    }
}
