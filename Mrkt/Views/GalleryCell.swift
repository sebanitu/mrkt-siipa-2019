//
//  GalleryCell.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 21/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    @IBOutlet weak var galleryImage: UIImageView!
}
