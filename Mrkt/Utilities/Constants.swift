//
//  Constants.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 12/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import Foundation

let LISTINGS_REF = "listings"
let USERS_REF = "users"

let BRAND = "brand"
let DESCRIPTION = "desciption"
let NAME = "name"
let PRICE = "price"
let TIMESTAMP = "timestamp"
let STATUS = "status"
let IMAGES = "images"
let USERNAME = "username"
let DATE_CREATED = "dateCreated"
let OWNER_ID = "ownerId"
let PHONE_NUMBER = "phoneNumber"
let PROFILE_PIC_URL = "profilePicUrl"
let OFFERS = "offers"
