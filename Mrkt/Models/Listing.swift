//
//  Listing.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 12/01/2020.
//  Copyright © 2020 Sebastian Nitu. All rights reserved.
//

import Foundation
import Firebase

class Listing {
    private(set) var brand: String
    private(set) var description: String
    private(set) var name: String
    private(set) var price: Int
    private(set) var status: Int
    private(set) var timestamp: Date
    private(set) var documentId: String
    private(set) var ownerId: String
    private(set) var imageURLs: [String]?
    private(set) var offers: [String]?
    
    init(brand: String, description: String, name: String, price: Int, status: Int, timestamp: Date, documentId: String, ownerId: String, imageURLs: [String], offers: [String]) {
        self.brand = brand
        self.description = description
        self.documentId = documentId
        self.name = name
        self.price = price
        self.status = status
        self.timestamp  = timestamp
        self.ownerId = ownerId
        self.imageURLs = imageURLs
        self.offers = offers
    }
    
    class func parseData(snapshot: QuerySnapshot?) -> [Listing] {
        var listings = [Listing]()
        guard let snap = snapshot else { return listings }
                 for document in snap.documents {
                     let data = document.data()
                     let brand = data[BRAND] as? String ?? "-"
                     let description = data[DESCRIPTION] as? String ?? ""
                     let name = data[NAME] as? String ?? ""
                     let price = data[PRICE] as? Int ?? 0
                     let status = data[STATUS] as? Int ?? 0
                     let timestamp = data[TIMESTAMP] as? Date ?? Date()
                     let documentId = document.documentID
                     let ownerId = document[OWNER_ID] as? String ?? ""
                     let offers = document.get(OFFERS) as? [String] ?? [""]
                    
                    // TODO: Find a better way to retrieve Image Arrays
        
                     let imageURLs = document[IMAGES] as? [String] ?? [""]
                     
                    let newListing = Listing(brand: brand, description: description, name: name, price: price, status: status, timestamp: timestamp, documentId: documentId, ownerId: ownerId, imageURLs: imageURLs, offers: offers)
                     listings.append(newListing)
                  }
        return listings
    }
}
