//
//  AppDelegate.swift
//  Mrkt
//
//  Created by Sebastian Nitu on 30/10/2019.
//  Copyright © 2019 Sebastian Nitu. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var userInformation: Dictionary<String, Any> = [:]

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        return true
    }
}

